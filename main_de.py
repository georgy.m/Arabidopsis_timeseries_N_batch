import matplotlib.pyplot as plt
from functools import reduce
import datatable as dt
import seaborn as sns
import pandas as pd
import numpy as np
import os 


# Number of genes to select for GO affinity testing
num_genes = 500
folder = 'output'

# filename = 'de.txt'
# df = dt.fread(filename).to_pandas()
# df = df.set_index(df.columns[0])
# df.index.name = 'GENE_ID'

# ns = ['01:00', '03:00', '05:00', '07:00', '09:00', '11:00', '13:00', '15:00',
#       '17:00', '19:00', '21:00', '23:00']
# times = list(map(lambda x: x.split('_')[0], df.columns))
# d = dict()

# df = df < 0.05
# for time in times:
#     l = time + '_left'
#     r = time + '_right'
#     if time in ns:
#         time = time + 'N'
#     d[time] = (sum(df[l] | df[r]), sum(df[l] & df[r]))

# res = pd.DataFrame(d, index=['Union', 'Intersection'])
# palette = ['red' if c.endswith('N') else 'blue' for c in res.columns]
# plt.figure(figsize=(22, 10), dpi=200)
# plt.subplot(2,1,1)
# sns.barplot(data=res.drop('Union'), palette=palette)
# plt.xlabel(str())
# plt.ylabel(str())
# plt.xticks(rotation=50)
# plt.title('Intersection from DE left&right counts')
# plt.subplot(2,1,2)
# sns.barplot(data=res.drop('Intersection'), palette=palette)
# plt.xlabel(str())
# plt.ylabel(str())
# plt.xticks(rotation=50)
# plt.title('Union from left&right DE counts')


filename = 'de_join.txt'
df = dt.fread(filename).to_pandas()
df = df.set_index(df.columns[0])
df.index.name = 'GENE_ID'

filename = 'de_lfc_join.txt'
df_lf = dt.fread(filename).to_pandas()
df_lf = df_lf.set_index(df_lf.columns[0])
df_lf.index.name = 'GENE_ID'

ns = ['01:00', '03:00', '05:00', '07:00', '09:00', '11:00', '13:00', '15:00',
      '17:00', '19:00', '21:00', '23:00']
times = list(map(lambda x: x.split('_')[0], df.columns))
d = dict()

df = df < 0.05
df_lf = df_lf >= 0
for time in times:
    t = df[time]
    up = sum(t.loc[df_lf[time]])
    down = sum(t.loc[~df_lf[time]])
    if time in ns:
        time = time + 'N'
    d[time] = (up, -down)
sns.set(font_scale=0.95, rc={'figure.facecolor':'white',
                          'axes.facecolor':'white'})
res = pd.DataFrame(d, index=['Up','Down'])
palette = ['red' if c.endswith('N') else 'blue' for c in res.columns]
fig = plt.figure(figsize=(18, 10), dpi=200)
# _, ax = plt.subplots()
# plt.subplot(2,1,1)
sns.barplot(data=res.loc[['Up']], palette=palette)
sns.barplot(data=res.loc[['Down']], palette=palette)
plt.xlabel('DE genes count')
plt.ylabel(str())
plt.xticks(rotation=50)
fig.get_axes()[0].set_yticklabels([int(abs(i))
                                   for i in fig.get_axes()[0].get_yticks()])
plt.axhline(color='k', linewidth=0.25)
plt.savefig(os.path.join(folder, 'de_barplot.png'))

plt.figure(figsize=(10, 10), dpi=200)
sm = df.sum(axis=1)
sm2 = df[ns].sum(axis=1)
sm3 = df[filter(lambda x: x not in ns, df.columns)].sum(axis=1)
sns.histplot(sm)
sns.histplot(sm2, color='red')
plt.xticks(list(range(0, sm.max() + 1)))
plt.xlabel('Number of time frames')
plt.ylabel('Gene count')
plt.legend(labels=['All time', 'Only N-times'])
plt.savefig(os.path.join(folder, 'de_time_frames.png'))

from intermine.webservice import Service
service = Service("https://bar.utoronto.ca/thalemine/service")
template = service.get_template('Gene_GO')
from collections import defaultdict
from tqdm import tqdm

tdf = df
dfn = tdf[ns].sum(axis=1).sort_values(ascending=False)
gs = list(dfn.index)[:num_genes]
gos = defaultdict(set)
gene_gos = defaultdict(set)

print("Requesting GO annotations for genes...")
for gene in tqdm(gs):
    for row in template.rows(A={"op": "=", "value": gene}):
        go =  row["goAnnotation.ontologyTerm.name"]
        gos[go].add(gene)
        gene_gos[gene].add(go)

gos = {v: len(k) for v, k in gos.items()}
gos = pd.DataFrame(gos, index=['Gene count']).T.sort_values('Gene count',
                                                            ascending=False)
gos.to_csv(os.path.join(folder, 'gos.csv'))
columns = ['DE count', 'GO count'] + list(gos.index)
rows = list()
gene_df = list()
for gene, go in gene_gos.items():
    de_count = dfn[gene]
    go_count = len(go)
    lt = [de_count, go_count] + [c in go for c in columns[2:]]
    gene_df.append(lt)
    rows.append(gene)
gene_df = pd.DataFrame(gene_df, columns=columns, index=rows)
gene_df.to_csv(os.path.join(folder, 'genes_go.csv'))
