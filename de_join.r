library(edgeR)

filename = "raw_counts_filtered.txt"
# That's just time frame repeated twice (i.e. for individuals a&b). The only
# exception is 9:20, as one of the individuals had an "_S" postfix and was dropped
# from the analysis. This crazy string was not typed manually, but generated
# by Python and copy&pasted here.
gs = c('00:00', '00:00', '00:20', '00:20', '00:40', '00:40', '01:00', '01:00', '02:00', '02:00', '02:20', '02:20', '02:40', '02:40', '03:00', '03:00', '03:20', '03:20', '03:40', '03:40', '04:00', '04:00', '04:20', '04:20', '04:40', '04:40', '05:00', '05:00', '05:20', '05:20', '05:40', '05:40', '06:00', '06:00', '06:20', '06:20', '06:40', '06:40', '07:00', '07:00', '07:20', '07:20', '07:40', '07:40', '08:00', '08:00', '08:20', '08:20', '08:40', '08:40', '09:00', '09:00', '09:20', '09:40', '09:40', '10:00', '10:00', '10:20', '10:20', '10:40', '10:40', '11:00', '11:00', '11:20', '11:20', '11:40', '11:40', '12:00', '12:00', '12:20', '12:20', '12:40', '12:40', '13:00', '13:00', '13:20', '13:20', '13:40', '13:40', '14:00', '14:00', '14:20', '14:20', '14:40', '14:40', '15:00', '15:00', '15:20', '15:20', '15:40', '15:40', '16:00', '16:00', '16:20', '16:20', '16:40', '16:40', '17:00', '17:00', '17:20', '17:20', '17:40', '17:40', '18:00', '18:00', '18:20', '18:20', '18:40', '18:40', '19:00', '19:00', '19:20', '19:20', '19:40', '19:40', '20:00', '20:00', '20:20', '20:20', '20:40', '20:40', '21:00', '21:00', '21:20', '21:20', '21:40', '21:40', '22:00', '22:00', '22:20', '22:20', '22:40', '22:40', '23:00', '23:00', '23:20', '23:20', '23:40', '23:40'
)
df = read.csv(filename, 
              # I set check.names to FALSE as the reader corrupts column names
              # otherwise. R is weiRd
              check.names=FALSE,
              row.names=1, header=TRUE)
# That's just available time frames (or unique entries from "gs")
gt = c('00:00', '00:20', '00:40', '01:00', '02:00', '02:20', '02:40', '03:00', '03:20', '03:40', '04:00', '04:20', '04:40', '05:00', '05:20', '05:40', '06:00', '06:20', '06:40', '07:00', '07:20', '07:40', '08:00', '08:20', '08:40', '09:00', '09:20', '09:40', '10:00', '10:20', '10:40', '11:00', '11:20', '11:40', '12:00', '12:20', '12:40', '13:00', '13:20', '13:40', '14:00', '14:20', '14:40', '15:00', '15:20', '15:40', '16:00', '16:20', '16:40', '17:00', '17:20', '17:40', '18:00', '18:20', '18:40', '19:00', '19:20', '19:40', '20:00', '20:20', '20:40', '21:00', '21:20', '21:40', '22:00', '22:20', '22:40', '23:00', '23:20', '23:40')
ncols = c()
vals = matrix(nrow=nrow(df))
vals_fc = matrix(nrow=nrow(df))
genes = rownames(df)
n = length(gt) - 1
for (i in 2:n) # We don't go or 00:00 and 23:40 as they don't have a complete set of "neighbours"
{
  print(i)
  # Explanation: Here, I search column indices for left-side neighbours,
  # right-side neighbours and for both individuals at the current time frame (aka "control")
  # la, lb are indices of the first and the last respectively left-side neighbours
  # ra, rb - the same for the right side, and the same for ca, cb.
  t = gt[i]
  ca = match(t, gs)
  cb = ca
  while (gs[cb + 1] == t)
  {
    cb = cb + 1
  }
  ra = cb + 1
  rb = ra
  while (((rb + 1) <= length(gs)) && (gs[rb + 1] == gs[ra]))
  {
    rb = rb + 1
  }
  lb = ca - 1
  la = lb
  while (((la-1) > 0) && (gs[la - 1] == gs[lb]))
  {
    la = la - 1
  }
  if (la == lb)
  {
    left = df[, la]
  }
  else
  {
    left = df[, la:lb]
  }
  if (ca == cb)
  {
    control = df[, ca]
  }
  else
  {
    control = df[, ca:cb]
  }
  if (ra == rb)
  {
    right = df[, ra]
  }
  else
  {
    right = df[, ra:rb]
  }
  
  # Here, we join the control with test (aka left+right) 
  df_c = cbind(left, control, right)
  # 'control', 'control', 'test', 'test', 'control', 'control' usually
  groups = c(rep('control', lb - la + 1),
             rep('test', cb - ca + 1),
             rep('control', rb - ra + 1))
  dg = DGEList(df_c, group=groups)
  dg = calcNormFactors(dg)
  dg = estimateDisp(dg)
  tags = topTags(exactTest(dg, c('control', 'test')), n=nrow(df))$table[genes,]
  ncols = c(ncols, t)
  vals = cbind(vals, tags$FDR)
  vals_fc = cbind(vals_fc, tags$logFC)
}
vals = vals[, -1]
colnames(vals) = ncols
rownames(vals) = genes
vals_fc = vals_fc[, -1]
colnames(vals_fc) = ncols
rownames(vals_fc) = genes
write.csv(vals, "de_join.txt")
write.csv(vals_fc, 'de_lfc_join.txt')