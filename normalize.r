library(edgeR)
filename = "light_counts_filtered.txt"
df_c = read.delim(filename, row.names="GENE_ID", check.names=FALSE)


df = df_c
df = DGEList(df, group=colnames(df))
nf = calcNormFactors(df)
df_cpm = cpm(nf)
write.csv(df_cpm, "TMM_CPM.txt")
write.csv(nf$samples, "TMM_CPM_samples.txt")