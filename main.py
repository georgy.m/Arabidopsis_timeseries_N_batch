#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
from functools import reduce
import datatable as dt
import seaborn as sns
import pandas as pd
import numpy as np
import os

drop_s = False
drop_outliers = False
iqr_multiplier = 4
tail_high = 0.4
tail_low = -0.4
specie = 'a'
fractions_all = False


maximum = False # If False, then average of 2 neighbors is calculated

# Creating output folder
folder = 'output'
if not os.path.isdir(folder):
    os.mkdir(folder)

# Reading normalized count data
filename = 'TMM_CPM.txt'
df = dt.fread(filename).to_pandas()
df = df.set_index(df.columns[0])
df.index.name = 'GENE_ID'

# Dropping second sample from the analysis (optionally also dropping S-samples)
mapping = dict()
cols, ncols, cols_n = list(), list(), list()
flags = np.zeros(df.shape[1], dtype=bool)
for i, c in enumerate(df.columns):
    time, spc = c.split('_')
    if spc != specie:
        continue
    if time[-1] == 'N':
        flags[i] = True
        cols_n.append(time)
    cols.append(c)
    ncols.append(time)
df = df[cols]
df.columns = ncols
    


# Calculating logfold change statistics

def calc_stats(df: pd.DataFrame, cols: list, maximum=maximum) -> pd.DataFrame:
    stats = list()
    cs = list(df.columns)
    inds = [cs.index(c) for c in cols]
    x = df.values
    for i in inds:
        t = x[:, i]
        l = x[:, i - 1]
        r = x[:, i + 1]
        with np.errstate(invalid='ignore', divide='ignore'):
            tl = np.log10(t) - np.log10(l)
            tr = np.log10(t) - np.log10(r)
        tl[np.isinf(tl)] = np.nan
        tr[np.isinf(tr)] = np.nan
        if maximum:
            stats.append(np.nanmax(np.array([tl, tr]), axis=0))
        else:
            stats.append(np.nanmean(np.array([tl, tr]), axis=0))
    stats = np.array(stats).T
    return pd.DataFrame(stats, index=df.index, columns=cols)

all_stats = calc_stats(df, ncols[1:-1])
all_stats.to_csv(os.path.join(folder, 'foldchanges_rel.csv'))
## Extracting subframe with N-samples together with ok-samples with 40 min lag
## to ensure that no intersection occurs
def time_to_ints(s):
    h, m = s.split(':')
    if m.endswith('N'):
        m = m[:-1]
    return int(h), int(m)
def ints_to_time(h, m):
    while m < 0:
        h -= 1
        m = 60 - m
    h += m // 60
    m = m % 60
    while h < 0:
        h += 24
    h = h % 24
    return '{:02d}:{:02d}'.format(h, m)
    
cs = set(all_stats.columns)
subcols = list()
for c in cols_n:
    h, m = time_to_ints(c)
    prev = ints_to_time(h - 1, 20)
    if prev in cs and prev not in subcols:
        subcols.append(prev)
    if c not in subcols:
        subcols.append(c)  
sub_stats = all_stats[subcols]

## Same but also with 40 min in future when applicable
subcols = list()
for c in cols_n:
    h, m = time_to_ints(c)
    prev = ints_to_time(h - 1, 20)
    if prev in cs and prev not in subcols:
        subcols.append(prev)
    if c not in subcols:
        subcols.append(c)  
    nxt = ints_to_time(h + 1, 40)
    if nxt in cs and nxt not in subcols:
        subcols.append(nxt)
sub_stats2 = all_stats[subcols]

# Dropping extreme outliers for better visuals
orig_all_stats = all_stats.copy()
if drop_outliers:
    q1 = np.nanpercentile(all_stats.values, 25, 1)
    q3 = np.nanpercentile(all_stats.values, 75, 1)
    iqr = q3 - q1
    outliers = np.any(all_stats.values.T > q3 + iqr_multiplier * iqr, axis=0)
    outliers = np.array(df.index)[outliers]
    all_stats = all_stats.drop(outliers)
    sub_stats = sub_stats.drop(outliers)
    sub_stats2 = sub_stats2.drop(outliers)
    print('N outliers:', len(outliers))
    with open(os.path.join(folder, 'outliers_iqr.txt'), 'w') as f:
        f.writelines(outliers)

# Violin plots
plt.figure(figsize=(64,16), dpi=300)
sns.set(font_scale=4, rc={'figure.facecolor':'white',
                          'axes.facecolor':'white'})
palette = ['red' if c in cols_n else 'blue' for c in sub_stats.columns]
sns.violinplot(data=sub_stats, palette=palette)
plt.ylabel('Average logfoldchange across neighbors')
plt.xticks(rotation=50)
plt.tight_layout()
plt.savefig(os.path.join(folder, 'violin.png'), dpi=300)

# Barplots
plt.figure(figsize=(64, 18), dpi=300)
sns.set(font_scale=3.8, rc={'figure.facecolor':'white',
                          'axes.facecolor':'white'})
palette = ['red' if c in cols_n else 'blue' for c in all_stats.columns]
sns.barplot(data=all_stats, palette=palette)
plt.ylabel('Average logfoldchange across neighbors')
plt.xlabel('Time (h:m)')
plt.xticks(rotation=50)
plt.tight_layout()
plt.savefig(os.path.join(folder, 'barplot.png'), dpi=300)


# Scatterplots + Marginals (same axes)
def abline():
    gca = plt.gca()
    gca.set_autoscale_on(False)
    gca.plot(gca.get_xlim(),gca.get_ylim(), 'k--')
plt.figure(figsize=(70, 100), dpi=200)
sns.set(font_scale=2, rc={'figure.facecolor':'white',
                          'axes.facecolor':'white', 
                          "font.size": 22,
                          "axes.titlesize": 64,
                          "axes.labelsize": 48})
cols = list(sub_stats2.columns)
for i, c in enumerate(cols_n):
    j = cols.index(c)
    nlc = sub_stats2[cols[j]]
    left = sub_stats2[cols[j - 1]]
    if j + 1 < len(cols):
        right = sub_stats2[cols[j + 1]]
    else:
        right = None
    plt.subplot(len(cols_n), 5, 5 * i + 1)
    sns.kdeplot(nlc)
    sns.kdeplot(left)
    if right is not None:
        sns.kdeplot(right)
    plt.ylabel(c)
    plt.xlabel(str())
    if i == 0:
        plt.title('KDE')
    plt.subplot(len(cols_n), 5, 5 * i + 2)
    sns.kdeplot(nlc - np.nanmean(nlc))
    sns.kdeplot(left - np.nanmean(left))
    if right is not None:
        sns.kdeplot(right - np.nanmean(right))
    plt.ylabel(str())
    plt.xlabel(str())
    plt.yticks([])
    if right is not None:
        plt.legend(labels=['N', 'N-40min', 'N+40min'])
    else:
        plt.legend(labels=['N', 'N-40min'])
    if i == 0:
        plt.title('KDE (centered)')
    plt.subplot(len(cols_n), 5, 5 * i + 3)
    sns.scatterplot(x=left, y=nlc)
    plt.ylabel(str())
    plt.xlabel(str())
    abline()
    if i == 0:
        plt.title('N vs -40min')
    if right is not None:
        plt.subplot(len(cols_n), 5, 5 * i + 4)
        sns.scatterplot(x=right, y=nlc)
        abline()
        plt.yticks([])
        plt.ylabel(str())
        plt.xlabel(str())
        if i == 0:
            plt.title('N vs +40min')
        plt.subplot(len(cols_n), 5, 5 * i + 5)
        sns.scatterplot(x=left, y=right)
        abline()
        plt.ylabel(str())
        plt.xlabel(str())
        if i == 0:
            plt.title('-40min vs +40min')
plt.tight_layout()
plt.savefig(os.path.join(folder, 'marginals_scatter.png'), dpi=200)


# Building KDE plots with filled tails together with number of
# outlier-like genes (those that fail in tails)
subfolder = os.path.join(folder, 'tails')
if not os.path.isdir(subfolder):
    os.mkdir(subfolder)
genes_high = dict()
genes_low = dict()
gs = np.array(list(orig_all_stats.index))
for _, v in orig_all_stats.iteritems():
    plt.figure(figsize=(10, 10), dpi=100)
    ax = sns.kdeplot(v)
    x, y = ax.get_lines()[0].get_data()
    mask1 = x < tail_low
    mask2 = x > tail_high
    if sum(mask1):
        mx, my = x[mask1], y[mask1]
        ax.fill_between(mx, my, facecolor='green')
        mask = v < tail_low
        genes_low[v.name] = set(gs[mask])
        plt.text(np.mean(mx), my[-1] / 2, sum(mask), fontsize=24)
    else:
        genes_low[v.name] = set()
    if sum(mask2) > 4:
        mx, my = x[mask2], y[mask2]
        ax.fill_between(mx, my, facecolor='red')
        mask = v > tail_high
        genes_high[v.name] = set(gs[mask])
        plt.text(mx[3], my[0] / 2, sum(mask), fontsize=24)
    else:
        genes_high[v.name] = set()
    plt.xlabel(str())
    plt.ylabel(str())
    plt.title(v.name)
    plt.savefig(os.path.join(subfolder, v.name.replace(':', '_')),
                dpi=100)
genes_high_total = reduce(set.__or__, genes_high.values())
genes_low_total = reduce(set.__or__, genes_low.values())
genes_total = genes_low_total | genes_high_total
genes = dict()
for t, gs in genes_high.items():
    genes[t] = (len(genes_low[t]), len(gs))
fname = os.path.join(folder, 'tail_counts.csv')
pd.DataFrame(genes, index=['Left tail', 'Right tail']).T.to_csv(fname)

# Plotting tail cutoff value dependency on gene count
## For N
ps = dict()
for val in np.arange(0.005, 2, 0.005):
    genes_high = dict()
    genes_low = dict()
    gs = np.array(list(orig_all_stats.index))
    for _, v in orig_all_stats.iteritems():
        if v.name not in cols_n:
            continue
        mask = v < -val
        genes_low[v.name] = set(gs[mask])
        mask = v > val
        genes_high[v.name] = set(gs[mask])
    genes_high_total = reduce(set.__or__, genes_high.values())
    genes_low_total = reduce(set.__or__, genes_low.values())
    genes_total = genes_high_total | genes_low_total
    n = len(gs)
    ps[val] = (len(genes_total) / n, len(genes_low_total)/ n,
               len(genes_high_total) / n, )

df_n = pd.DataFrame(ps, index=['Both', 'Left tail', 'Right tail']).T
## For those that have no Ns in a neighbourhood
cols = list(filter(lambda x: not x.endswith('N'), sub_stats2.columns))
if not fractions_all:
    cols = cols[::2]
ps = dict()
for val in np.arange(0.005, 2, 0.005):
    genes_high = dict()
    genes_low = dict()
    gs = np.array(list(orig_all_stats.index))
    for _, v in orig_all_stats[cols].iteritems():
        mask = v < -val
        genes_low[v.name] = set(gs[mask])
        mask = v > val
        genes_high[v.name] = set(gs[mask])
    genes_high_total = reduce(set.__or__, genes_high.values())
    genes_low_total = reduce(set.__or__, genes_low.values())
    genes_total = genes_high_total | genes_low_total
    n = len(gs)
    ps[val] = (len(genes_total) / n, len(genes_low_total)/ n,
               len(genes_high_total) / n, )
df_no_n = pd.DataFrame(ps, index=['Both', 'Left tail', 'Right tail']).T
plt.figure(figsize=(25, 22), dpi=150)
sns.set(font_scale=2, rc={'figure.facecolor':'white',
                          'axes.facecolor':'white', 
                          "font.size": 22,
                          "axes.titlesize": 32,
                          "axes.labelsize": 24})
plt.subplot(2, 2, 1)
sns.lineplot(data=df_n)
plt.ylim(0, 0.5)
plt.ylabel('Fraction of genes')
plt.xlabel(str())
plt.xticks([])
plt.title('N-batches')
plt.subplot(2, 2, 2)
sns.lineplot(data=df_no_n)
plt.legend([],[], frameon=False)
plt.ylim(0, 0.5)
plt.ylabel(str())
plt.xlabel(str())
plt.xticks([])
plt.yticks([])
plt.title('No N-batches')
plt.subplot(2, 2, 3)
sns.lineplot(data=pd.DataFrame([df_n['Left tail'], df_no_n['Left tail']],
                               index=['N', 'No N']).T,
             palette=['Blue', 'Red'], dashes=False)
plt.ylim(0, 0.5)
plt.ylabel('Fraction of genes')
plt.xlabel('Tail cutoff')
plt.title('Left tail (N vs No-N)')
plt.subplot(2, 2, 4)
sns.lineplot(data=pd.DataFrame([df_n['Right tail'], df_no_n['Right tail']],
                               index=['N', 'No N']).T,
             palette=['Blue', 'Red'], dashes=False)
plt.legend([],[], frameon=False)
plt.ylim(0, 0.5)
plt.ylabel(str())
plt.xlabel('Tail cutoff')
plt.yticks([])
plt.title('Right tail (N vs No-N)')
plt.tight_layout()
plt.savefig(os.path.join(folder, 'fraction.png'), dpi=150)


#
# tc = list(filter(lambda x: not x.endswith('N'), sub_stats.columns))
# vn = all_stats[cols_n].mean(axis=1)
# v = all_stats[cols].mean(axis=1)
# plt.figure(figsize=(22, 10), dpi=200)
# plt.subplot(1, 2, 1)
# sns.kdeplot(vn)
# sns.kdeplot(v)
# plt.legend(labels=['N', 'No_N'])
# plt.subplot(1, 2, 2)
# sns.scatterplot(x=v, y=vn)
# abline()
# plt.ylabel('N')
# plt.xlabel('No N')