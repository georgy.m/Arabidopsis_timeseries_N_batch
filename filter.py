#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
from functools import reduce
import datatable as dt
import seaborn as sns
import pandas as pd
import numpy as np
import os



filename = 'light_series.txt'
cpm_cutoff = 1.0
raw_counts = dt.fread(filename).to_pandas()
raw_counts = raw_counts[filter(lambda x: not x.endswith('S'),
                               raw_counts.columns)]
raw_counts = raw_counts.set_index(raw_counts.columns[0])
raw_counts = raw_counts.loc[raw_counts.max(axis=1) > 0]
cpm = (raw_counts / raw_counts.sum(axis=0)) * 1e6

mapping = dict()
times = set()

for c in cpm.columns:
    parts = c.split('_')
    a = parts[0]
    specie = a[-1]
    try:
        a, b = a.split('.')
    except ValueError:
        a, b = a.split('-')
    h = int(a[2:])
    m = int(b[:-1])
    s = '{:02d}:{:02d}'.format(h, (m - 1) * 20)
    times.add(s)
    if parts[-1] == 'N':
        mapping[c] = s + 'N_' + specie
    else:
        mapping[c] = s + '_' + specie


cols, ncols = [], []
for a, b in mapping.items():
    cols.append(a)
    ncols.append(b)
cpm = cpm[cols]
raw_counts = raw_counts[cols]
cpm.columns = ncols
raw_counts.columns = ncols
# tcols = cols
# Filter
mask = None

for i, time in enumerate(times):
    try:
        j = ncols.index(time + '_a')
    except ValueError:
        j = ncols.index(time + 'N_a')
    cols = ncols[max([0, j-1, j-2]):min([j + 1, j + 2, j + 3, len(ncols)])]
    m = np.any(cpm[cols] > cpm_cutoff, axis=1)
    if mask is None:
        mask = m
    else:
        mask |= m

raw_counts = raw_counts.loc[mask]
# raw_counts.columns = tcols
raw_counts.to_csv("raw_counts_filtered.txt", sep=',')




# for time in cols:


